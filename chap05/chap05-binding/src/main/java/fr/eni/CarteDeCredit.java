package fr.eni;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;


/**
 * Carte de crédit
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarteDeCredit {
  /**
   * Premier quadruplet
   */
  private int premierQuadruplet;

  /**
   * Second quadruplet
   */
  private int secondQuadruplet;

  /**
   * Troisième quadruplet
   */
  private int troisiemeQuadruplet;

  /**
   * Quatrième quadruplet
   */
  private int quatriemeQuadruplet;
}

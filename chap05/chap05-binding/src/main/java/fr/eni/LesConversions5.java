package fr.eni;

import org.springframework.core.convert.support.GenericConversionService;


/**
 * Exemples sur les conversions avec UtilisateurToStringConverterFactory et 
 * StringToUtilisateurConverterFactory 
 *
 * @author $author$
 * @version $Revision$
  */
public class LesConversions5 {
  /**
   * Point d'entrée
   *
   * @param args Les arguments de la ligne de commande
   */
  public static void main(String[] args) {
    GenericConversionService conversionService = new GenericConversionService();

    conversionService.addConverterFactory(new UtilisateurToStringConverterFactory());
    conversionService.addConverterFactory(new StringToUtilisateurConverterFactory());

    String utilisateurAsString = "7,Sarah,CONNOR";
    Utilisateur utilisateur = conversionService.convert(utilisateurAsString,
        Utilisateur.class);

    System.out.println("Le nom est " + utilisateur.getNom());
    System.out.println("Le prénom est " + utilisateur.getPrenom());

    utilisateurAsString = conversionService.convert(utilisateur, String.class);
    System.out.println("Utilisateue sous forme de chaine [" +
      utilisateurAsString + "]");
  }
}

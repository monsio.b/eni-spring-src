package fr.eni.services;

import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.ResourceLoaderAware;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.util.Locale;


/**
 * La casse de suppport
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class RessourceHelperService implements MessageSourceAware {
  /**
   * Le messageSource
   */
  private MessageSource messageSource;

  /**
   * Setter du messageSource
   *
   * @param messageSource La messageSource
   */
  public void setMessageSource(MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  /**
   * Affichage des messages
   */
  public void afficherMessages() {
    String name = messageSource.getMessage("utilisateur.nom",
        new Object[] { "John", 25, "Dallas" }, Locale.US);

    log.info("User name (English) : " + name);

    name = messageSource.getMessage("utilisateur.nom",
        new Object[] { "John", 25, "Dallas" }, Locale.US);

    log.info("Nom utilisateur (France) : " + name);
  }
}

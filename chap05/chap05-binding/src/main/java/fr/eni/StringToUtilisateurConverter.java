package fr.eni;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;


/**
 * Exemple conversion chaîne vers utilisateur
 *
 * @author $author$
 * @version $Revision$
  */
public class StringToUtilisateurConverter implements Converter<String, Utilisateur> {
  /**
   * Conversion
   *
   * @param UtilisateurAsString Utilisateur sous forme de String
   *
   * @return L'utilisateur
   */
  @Override
  public Utilisateur convert(String UtilisateurAsString) {
    if (UtilisateurAsString == null) {
      throw new ConversionFailedException(TypeDescriptor.valueOf(String.class),
        TypeDescriptor.valueOf(String.class), UtilisateurAsString, null);
    }

    String[] tempArray = UtilisateurAsString.split(",");
    Utilisateur article = new Utilisateur(Integer.parseInt(tempArray[0]),
        tempArray[1], tempArray[2]);

    return article;
  }
}

package fr.eni;

import org.springframework.format.Parser;

import java.text.ParseException;

import java.util.Locale;


/**
 * La parser de carte de crédit
 *
 * @author $author$
 * @version $Revision$
  */
public class CarteDeCreditParser implements Parser {
  /**
   * parsing
   *
   * @param cc La carte de crédit
   * @param locale La locale
   *
   * @return Une carte de crédit
   *
   * @throws ParseException Exception levée
   */
  @Override
  public CarteDeCredit parse(String cc, Locale locale)
    throws ParseException {
    String[] caracteres = cc.split("-");

    if ((caracteres == null) || (caracteres.length != 4)) {
      throw new org.springframework.expression.ParseException(-1,
        "Invalid format");
    }

    CarteDeCredit c = new CarteDeCredit();
    c.setPremierQuadruplet(Integer.parseInt(caracteres[0]));
    c.setSecondQuadruplet(Integer.parseInt(caracteres[1]));
    c.setTroisiemeQuadruplet(Integer.parseInt(caracteres[2]));
    c.setQuatriemeQuadruplet(Integer.parseInt(caracteres[3]));

    return c;
  }
}

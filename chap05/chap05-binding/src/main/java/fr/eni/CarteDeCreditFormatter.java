package fr.eni;

import org.springframework.format.Formatter;
import org.springframework.format.Parser;
import org.springframework.format.Printer;

import java.text.ParseException;

import java.util.Locale;


/**
 * Le formatteur de carte de crédit
 *
 * @author $author$
 * @version $Revision$
  */
public class CarteDeCreditFormatter implements Formatter {
  /**
   * Le parser
   */
  private Parser parser;

  /**
   * Le printer
   */
  private Printer printer;

  /**
   * Création d'un formateur de carte de crédit
   *
   * @param parser Le parser
   * @param printer Le printer
   */
  public CarteDeCreditFormatter(Parser parser, Printer printer) {
    this.parser = parser;
    this.printer = printer;
  }

  /**
   * Parse du numéro de cate de crédit
   *
   * @param cc Carte de crédit
   * @param l la locale
   *
   * @return Une carte de crédit
   *
   * @throws ParseException Exception levée
   */
  @Override
  public CarteDeCredit parse(String cc, Locale l) throws ParseException {
    return (CarteDeCredit) parser.parse(cc, l);
  }

  /**
   * Printer
   *
   * @param cc La carte de crédit
   * @param l La locale
   *
   * @return Le numéro de carte de crédit dans une chaine de caractères
   */
  @Override
  public String print(Object cc, Locale l) {
    return printer.print(cc, l);
  }
}

package fr.eni;

import org.springframework.format.Printer;

import java.util.Locale;


/**
 * Le printer
 *
 * @author $author$
 * @version $Revision$
  */
public class CarteDeCreditPrinter implements Printer {
  /**
   * Affichage du numéro
   *
   * @param cc La carte de crédit
   * @param locale La locale
   *
   * @return Le numéro sous forme de chaine de caractères
   */
  @Override
  public String print(Object cc, Locale locale) {
    CarteDeCredit objet = (CarteDeCredit) cc;

    return objet.toString();
  }
}

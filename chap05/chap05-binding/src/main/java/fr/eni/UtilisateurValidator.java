package fr.eni;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


/**
 * Validateur
 *
 * @author $author$
 * @version $Revision$
  */
public class UtilisateurValidator implements Validator {
  /**
   * Supports
   *
   * @param clazz Le nom de la classe
   *
   * @return Egalité de classes
   */
  public boolean supports(Class clazz) {
    return Utilisateur.class.equals(clazz);
  }

  /**
   * Validation
   *
   * @param obj L'objet
   * @param e Les erreurs
   */
  public void validate(Object obj, Errors e) {
    ValidationUtils.rejectIfEmpty(e, "nom", "nom.vide");

    Utilisateur p = (Utilisateur) obj;

    if (p.getNom().contains("s")) {
      e.rejectValue("nom", "contient.s");
    } else if (p.getId() < 0) {
      e.rejectValue("id", "id.neg");
    }
  }
}

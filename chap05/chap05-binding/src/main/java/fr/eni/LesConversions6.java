package fr.eni;

import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.ConversionServiceFactory;
import org.springframework.core.convert.support.GenericConversionService;

import org.springframework.format.Formatter;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.number.NumberFormatter;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Exemples sur les formatteurs de dates et de nombres
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class LesConversions6 {
  /**
   * Point d'entrée
   *
   * @param args Les arguments de la ligne de commande
   *
   * @throws Exception L'exception levée
   */
  public static void main(String[] args) throws Exception {
    testDateFormatter();
    testNumberFormatter();
  }

  /**
   * Mise en forme de la date
   */
  private static void testDateFormatter() {
    Formatter dateFormatter = new DateFormatter();
    String dateAsString = dateFormatter.print(new Date(), Locale.FRANCE);
    System.out.println("Date en France : " + dateAsString);
  }

  /**
   * Mise en forme du nombre
   *
   * @throws Exception Exception levée
   */
  private static void testNumberFormatter() throws Exception {
    NumberFormatter doubleFormatter = new NumberFormatter();
    doubleFormatter.setPattern("#####.###");

    String number = doubleFormatter.print(new Double(12345.6789d), Locale.FRANCE);
    System.out.println("Nombre : " + number);
  }
}

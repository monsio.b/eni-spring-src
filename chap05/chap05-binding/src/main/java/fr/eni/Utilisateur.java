package fr.eni;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * Classe utilisateur
 *
 * @author $author$
 * @version $Revision$
  */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Utilisateur {
  /**
   * L'id
   */
  private long id;

  /**
   * Le prénom
   */
  private String prenom;

  /**
   * Le nom
   */
  private String nom;
}

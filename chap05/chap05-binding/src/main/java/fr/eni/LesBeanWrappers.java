package fr.eni;

import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.TypeMismatchException;

import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.net.BindException;

import java.util.List;


/**
 * Exemple de wrapper sur les Beans
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class LesBeanWrappers {
  /**
   * Point d'entrée
   *
   * @param args Arguments de la ligne de commande
   **/	
  public static void main(String[] args) {
    //Utilisation du BeanWrapper
    Utilisateur utilisateur = new Utilisateur();
    BeanWrapper bw = new BeanWrapperImpl(utilisateur);
    bw.setPropertyValue("nom", "John");
    bw.setPropertyValue("prenom", "DO");
    bw.setPropertyValue("id", "200");

    final int id = 200;
    log.info("Vérification :" + (id == utilisateur.getId()) + "Utilisateur=" +
      utilisateur);

    try {
      bw.setPropertyValue("id", "pas numérique");
    } catch (TypeMismatchException e) {
      log.info("Err binding :" + e.getMessage());
    }

    //Un peu plus complexe
    log.info("MutablePropertyValues:");

    MutablePropertyValues values = new MutablePropertyValues();
    values.addPropertyValue("nom", "DOs");
    values.addPropertyValue("prenom", "Jane");

    //values.addPropertyValue("id", "2100");
    DataBinder dataBinder = new DataBinder(utilisateur, "John");
    dataBinder.setAllowedFields(new String[] { "nom", "prenom" });
    dataBinder.setValidator(new UtilisateurValidator());
    dataBinder.bind(values);
    dataBinder.validate();

    log.info("allErrors:");

    List<ObjectError> allErrors = dataBinder.getBindingResult().getAllErrors();

    for (Object object : allErrors) {
      ObjectError error = (ObjectError) object;
      log.info(error.toString());
    }
  }
}

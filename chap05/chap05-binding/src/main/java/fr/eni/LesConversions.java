package fr.eni;

import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.support.ConversionServiceFactory;
import org.springframework.core.convert.support.GenericConversionService;

import java.util.List;


/**
 * Exemples sur les conversions des types courants
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class LesConversions {
  /**
   * Point d'entrée
   *
   * @param args Les arguments de la ligne de commande
   */
  public static void main(String[] args) {
    GenericConversionService conversionService = ConversionServiceFactory.createDefaultConversionService();

    testToArray(conversionService);
    testToCollection(conversionService);
    testToBoolean(conversionService);
    testToCharacter(conversionService);
    testToNumber(conversionService);
    testToEnum(conversionService);
  }

  /**
   * Conversion vers un Array
   *
   * @param conversionService Le service
   */
  private static void testToArray(GenericConversionService conversionService) {
    String[] stringArray = conversionService.convert("Un,Deux,Trois",
        String[].class);

    for (String element : stringArray) {
      log.info("Les éléments : " + element);
    }
  }

  /**
   * Conversion vers une collection
   *
   * @param conversionService Le service
   */
  private static void testToCollection(
    GenericConversionService conversionService) {
    @SuppressWarnings("unchecked")
    List<String> listOfStrings = conversionService.convert("Un,deux,Trois",
        List.class);

    for (String element : listOfStrings) {
      log.info("Les éléments " + element);
    }
  }

  /**
   * Conversion vers un booleen
   *
   * @param conversionService Le service
   */
  private static void testToBoolean(GenericConversionService conversionService) {
    Boolean data = null;

    data = conversionService.convert("true", Boolean.class);
    log.info("La valeur du booléen est " + data);

    data = conversionService.convert("no", Boolean.class);
    log.info("La valeur du booléen est " + data);
  }

  /**
   * Conversion vers un caractère
   *
   * @param conversionService Le service
   */
  private static void testToCharacter(
    GenericConversionService conversionService) {
    Character data = null;

    data = conversionService.convert("A", Character.class);
    log.info("Le caractère est " + data);

    /*
                    Exception in thread "main" org.springframework.core.convert.ConversionFailedException: Failed to convert from type java.lang.String to type java.lang.Character for value 'Exception'; nested exception is java.lang.IllegalArgumentException: Can only convert a [String] with length of 1 to a [Character]; string value 'Exception'  has length of 9
    */
    try {
      data = conversionService.convert("Exception", Character.class);
      log.info("Le caractère est " + data);
    } catch (ConversionFailedException e) {
      log.info("On veut catcher l'exception ConversionFailedException :" + e);
    }
  }

  /**
   * Conversion vers un Number
   *
   * @param conversionService Le service
   */
  private static void testToNumber(GenericConversionService conversionService) {
    Integer intData = conversionService.convert("124", Integer.class);
    log.info("La valeur de l'entier est " + intData);

    Float floatData = conversionService.convert("215f", Float.class);
    log.info("La valeur du flottant est " + floatData);
  }

  /**
   * Conversion vers un enum
   *
   * @param conversionService Le service
   */
  private static void testToEnum(GenericConversionService conversionService) {
    TaskStatus taskStatus = conversionService.convert("PENDING",
        TaskStatus.class);
    log.info("Le status de la tache est " + taskStatus);
  }
}

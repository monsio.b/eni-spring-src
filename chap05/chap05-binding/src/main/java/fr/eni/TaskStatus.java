package fr.eni;

public enum TaskStatus {
    STARTED, COMPLETED, PENDING;
}

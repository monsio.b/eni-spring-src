package fr.eni;

import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.ConversionServiceFactory;
import org.springframework.core.convert.support.GenericConversionService;

import java.util.Arrays;
import java.util.List;


/**
 * Conversion en utilisant des factories 
 * UtilisateurToStringConverterFactory et StringToUtilisateurConverterFactory
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class Conversions6 {
  /**
   * Point d'entrée
   *
   * @param args Arguments de la ligne de commande
   */	
	public static void main(String[] args) {

    GenericConversionService conversionService = new GenericConversionService();

    conversionService.addConverterFactory(new UtilisateurToStringConverterFactory());
    conversionService.addConverterFactory(new StringToUtilisateurConverterFactory());

    String utilisateurAsString = "5,Janelle,VOIGHT";
    Utilisateur utilisateur = conversionService.convert(utilisateurAsString,
        Utilisateur.class);
    System.out.println("Le nom est " + utilisateur.getNom());
    System.out.println("Le prénom est " + utilisateur.getPrenom());

    utilisateurAsString = conversionService.convert(utilisateur, String.class);
    System.out.println("Utilisateur en chaine : [" + utilisateurAsString + "]");
  }
}

package fr.eni;

import org.springframework.core.convert.support.ConversionServiceFactory;
import org.springframework.core.convert.support.GenericConversionService;

import java.util.List;


/**
 * Exemple de conversion en utilisant le convertisseur intégré vers un Array
 *
 * @author $author$
 * @version $Revision$
  */
public class ConvIntegreVersArray {
  /**
   * Point d'entrée
   *
   * @param args Arguments de la ligne de commande
   */	
  public static void main(String[] args) {
    GenericConversionService conversionService = ConversionServiceFactory.createDefaultConversionService();

    testToCollection(conversionService);
    testToString(conversionService);
    testToObject(conversionService);
  }

  /**
   * Conversion vers une collection
   *
   * @param conversionService Le service de conversion
   */
  private static void testToCollection(
    GenericConversionService conversionService) {
    @SuppressWarnings("unchecked")
    List<String> listeDeCouleurs = conversionService.convert(new String[] {
          "Rouge", "Vert", "Bleu"
        }, List.class);

    for (String color : listeDeCouleurs) {
      System.out.println("La couleur est " + color);
    }
  }

  /**
   * Conversion vers une chaine de caractères
   *
   * @param conversionService Le service de conversion
   */
  private static void testToString(GenericConversionService conversionService) {
    String colors = conversionService.convert(new String[] {
          "Rouge", "Vert", "Bleu"
        }, String.class);
    System.out.println("La couleur est " + colors);
  }

  /**
   * Conversion vers un objet
   *
   * @param conversionService Le service de conversion
   */
  private static void testToObject(GenericConversionService conversionService) {
    conversionService.addConverter(new StringToUtilisateurConverter());

    Utilisateur article = conversionService.convert(new String[] { "1,John,SMITH" },
        Utilisateur.class);
    System.out.println("Le nom est :" + article.getNom());
    System.out.println("Le prénom est :" + article.getPrenom());
  }
}

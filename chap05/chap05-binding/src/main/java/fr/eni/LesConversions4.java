package fr.eni;

import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.ConversionServiceFactory;
import org.springframework.core.convert.support.GenericConversionService;

import java.util.Arrays;
import java.util.List;


/**
 * Exemples sur les conversions 
 * UtilisateurToStringConverter et StringToUtilisateurConverter
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class LesConversions4 {
  /**
   * Point d'entrée
   *
   * @param args Les arguments de la ligne de commande
   */

  public static void main(String[] args) {
    test1();
    test2();
    test3();
  }

  /**
   * Conversion d'un utilisateur
   */
  private static void test1() {
    GenericConversionService conversionService = new GenericConversionService();

    Converter<Utilisateur, String> customConverter = new UtilisateurToStringConverter();
    conversionService.addConverter(customConverter);

    try {
      String result = customConverter.convert(null);
      System.out.println("Result is " + result);
    } catch (ConversionFailedException e) {
      log.info(e.toString());
    }
  }

  /**
   * Conversion d'un utilisateur
   */
  private static void test2() {
    GenericConversionService conversionService = new GenericConversionService();

    Converter<Utilisateur, String> customConverter = new UtilisateurToStringConverter();
    conversionService.addConverter(customConverter);

    Utilisateur article = new Utilisateur(3, "CONNOR", "Sarah");
    String result = conversionService.convert(article, String.class);
    System.out.println("Result is '" + result + "'");
  }

  /**
   * Conversion d'un utilisateur
   */
  private static void test3() {
    GenericConversionService conversionService = new GenericConversionService();

    Converter<String, Utilisateur> customConverter = new StringToUtilisateurConverter();
    conversionService.addConverter(customConverter);

    String articleAsString = new String("2,Kyle,REESE");
    Utilisateur result = conversionService.convert(articleAsString,
        Utilisateur.class);
    System.out.println("Le nom est " + result.getNom());
    System.out.println("Le prénom est " + result.getPrenom());
  }
}

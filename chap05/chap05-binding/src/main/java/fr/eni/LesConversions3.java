package fr.eni;

import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.support.ConversionServiceFactory;
import org.springframework.core.convert.support.GenericConversionService;

import java.util.Arrays;
import java.util.List;
import java.util.List;


/**
 * Exemples de conversion utilisant GenericConversionService pour les listes et objets
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class LesConversions3 {
  /**
   * Point d'entrée
   *
   * @param args Les arguments de la ligne de commande
   */
  public static void main(String[] args) {
    GenericConversionService conversionService = ConversionServiceFactory.createDefaultConversionService();

    testToArray(conversionService);
    testToString(conversionService);
    testToObject(conversionService);
  }

  /**
   * Conversion vers un Array
   *
   * @param conversionService Le service
   */
  private static void testToArray(GenericConversionService conversionService) {
    List languages = Arrays.asList("C", "C++", "Java");
    String[] stringArray = conversionService.convert(languages, String[].class);

    for (String language : stringArray) {
      System.out.println("Language is " + language);
    }
  }

  /**
   * Conversion vers une chaîne de caractères
   *
   * @param conversionService Le service
   */
  private static void testToString(GenericConversionService conversionService) {
    List languages = Arrays.asList("C", "C++", "Java");
    String languagesAsString = conversionService.convert(languages, String.class);
    System.out.println("All languages -->" + languagesAsString);
  }

  /**
   * Conversion vers un objet
   *
   * @param conversionService Le service
   */
  private static void testToObject(GenericConversionService conversionService) {
    conversionService.addConverter(new UtilisateurToStringConverter());

    Utilisateur utilObject = new Utilisateur(2, "CONNOR", "John");
    String utilAsString = conversionService.convert(new Utilisateur[] { utilObject },
        String.class);
    System.out.println("Utilisateur -->" + utilAsString);
  }
}

package fr.eni;

import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;


/**
 * La factory
 *
 * @author $author$
 * @version $Revision$
  */
public class UtilisateurToStringConverterFactory implements ConverterFactory<Utilisateur, String> {
  /**
   * Le convertisseur
   *
   * @param <T> Le type
   * @param arg0 l'utilisateur
   *
   * @return L'objet converti en fonction du type
   */
  @Override
  public <T extends String> Converter<Utilisateur, T> getConverter(
    Class<T> arg0) {
    return (Converter<Utilisateur, T>) new UtilisateurToStringConverter();
  }
}

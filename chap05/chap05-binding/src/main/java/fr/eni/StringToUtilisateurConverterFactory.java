package fr.eni;

import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;


/**
 * Conversion Chaine vers Utilisateur en utilisant une factory
 *
 * @author $author$
 * @version $Revision$
  */
public class StringToUtilisateurConverterFactory implements ConverterFactory<String, Utilisateur> {
  /**
   * Le convertisseur
   *
   * @param <T> Le type
   * @param arg0 la valeur
   *
   * @return L'objet retourné en fonction du type
   */
  @SuppressWarnings("unchecked")
  @Override
  public <T extends Utilisateur> Converter<String, T> getConverter(
    Class<T> arg0) {
    return (Converter<String, T>) new StringToUtilisateurConverter();
  }
}

package fr.eni;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;


/**
 * Conversion utilisateur vers String!
 *
 * @author $author$
 * @version $Revision$
  */
public class UtilisateurToStringConverter implements Converter<Utilisateur, String> {
  /**
   * Convertisseur
   *
   * @param utilisateur L'utilisateur
   *
   * @return La chaine de caractère
   */
  @Override
  public String convert(Utilisateur utilisateur) {
    if (utilisateur == null) {
      throw new ConversionFailedException(TypeDescriptor.valueOf(
          Utilisateur.class), TypeDescriptor.valueOf(String.class), utilisateur,
        null);
    }

    StringBuilder builder = new StringBuilder();
    builder.append(utilisateur.getId());
    builder.append(",");
    builder.append(utilisateur.getNom());
    builder.append(",");
    builder.append(utilisateur.getPrenom());

    return builder.toString();
  }
}

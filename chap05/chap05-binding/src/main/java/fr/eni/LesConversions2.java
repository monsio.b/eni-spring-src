package fr.eni;

import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.support.ConversionServiceFactory;
import org.springframework.core.convert.support.GenericConversionService;

import java.util.List;


/**
 * Exemples sur les conversions via le GenericConversionService pour des tablaux de Strings
 * Le createDefaultConversionService est déprécié mais montré pour l'ancien code.
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class LesConversions2 {
  /**
   * Point d'entrée
   *
   * @param args Les arguments de la ligne de commande
   */
  public static void main(String[] args) {
    GenericConversionService conversionService = ConversionServiceFactory.createDefaultConversionService();

    testToCollection(conversionService);
    testToString(conversionService);
    testToObject(conversionService);
  }

  /**
   * Conversion vers une collection
   *
   * @param conversionService Le service
   */
  private static void testToCollection(
    GenericConversionService conversionService) {
    @SuppressWarnings("unchecked")
    List<String> listOfTerm = conversionService.convert(new String[] {
          "t100", "t800", "tx"
        }, List.class);

    for (String term : listOfTerm) {
      System.out.println("Terminator is " + term);
    }
  }

  /**
   * Conversion vers une chaîne de caractères
   *
   * @param conversionService Le service
   */
  private static void testToString(GenericConversionService conversionService) {
    String terms = conversionService.convert(new String[] { "t100", "t800", "tx" },
        String.class);
    System.out.println("Terminator is " + terms);
  }

  /**
   * Conversion vers un objet
   *
   * @param conversionService Le service
   */
  private static void testToObject(GenericConversionService conversionService) {
    conversionService.addConverter(new StringToUtilisateurConverter());

    Utilisateur utilisateur = conversionService.convert(new String[] {
          "5,John,REESE"
        }, Utilisateur.class);
    System.out.println("Le nom est " + utilisateur.getNom());
    System.out.println("Le prénom est " + utilisateur.getPrenom());
  }
}

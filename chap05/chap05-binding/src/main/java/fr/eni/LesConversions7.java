package fr.eni;

import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.ConversionServiceFactory;
import org.springframework.core.convert.support.GenericConversionService;

import org.springframework.format.Formatter;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.number.NumberFormatter;
import org.springframework.format.support.FormattingConversionService;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Exemples sur les conversions d'un numéro de carte de crédit
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class LesConversions7 {
  /**
   * Point d'entrée
   *
   * @param args Les arguments de la ligne de commande
   */
  public static void main(String[] args) {
    FormattingConversionService service = new FormattingConversionService();
    CarteDeCreditParser parser = new CarteDeCreditParser();
    CarteDeCreditPrinter printer = new CarteDeCreditPrinter();
    service.addFormatterForFieldType(CarteDeCredit.class, printer, parser);
    test1(service);
    test2(service);
  }

  /**
   * Conversion numéro carte de crédit depuis une chaîne de caractères
   *
   * @param service Le service
   */
  private static void test1(FormattingConversionService service) {
    String cc = "1111-2222-3333-4444";
    CarteDeCredit o = (CarteDeCredit) service.convert(cc, CarteDeCredit.class);
    log.info(o.toString());
  }

  /**
   * Conversion numéro carte de crédit depuis une série de nombres
   *
   * @param service Le service
   */
  private static void test2(FormattingConversionService service) {
    CarteDeCredit o = new CarteDeCredit(1111, 2222, 3333, 4444);
    String cc = service.convert(o, String.class);
    log.info("Le no de la carte de crédit est :" + cc);
  }
}

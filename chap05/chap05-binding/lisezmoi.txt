Cet exemple montre les bindings.
Les classes porteuses des exemples sont les suivantes :

- Conversions6 : Conversion en utilisant des factories UtilisateurToStringConverterFactory et StringToUtilisateurConverterFactory
- ConvIntegreVersArray : Exemple de conversion en utilisant le convertisseur intégré vers un Array
- LesBeanWrappers :  Exemple de wrapper sur les Beans : Nous testons aussi les erreurs de validations.
- LesConversions : Les conversions des types courants
- LesConversions2 : Exemples sur les conversions via le GenericConversionService pour des tablaux de Strings
- LesConversions3 : Exemples de conversion utilisant GenericConversionService pour les listes et objets
- LesConversions4 : Exemples sur les conversions UtilisateurToStringConverter et StringToUtilisateurConverter
- LesConversions5 : Exemples sur les conversions avec UtilisateurToStringConverterFactory et StringToUtilisateurConverterFactory 
- LesConversions6 : Exemples sur les formatteurs de dates et de nombres
- LesConversions7 : Exemples sur les conversions d'un numéro de carte de crédit




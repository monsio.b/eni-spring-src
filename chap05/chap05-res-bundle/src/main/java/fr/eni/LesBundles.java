package fr.eni;

import fr.eni.services.RessourceHelperService;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;


/**
 * Classe montrant l'utilisation des bundles pour localiser les phrases.
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class LesBundles {
  /**
   * Point d'entrée
   *
   * @param args Arguments de la ligne de commande
   */
  public static void main(String[] args) throws SQLException {
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
        "applicationContext.xml");

    BeanFactory factory = (BeanFactory) context;

    String name = context.getMessage("utilisateur.nom",
        new Object[] { "John", 25, "Dallas" }, Locale.US);

    log.info("User name (English) : " + name);

    name = context.getMessage("utilisateur.nom",
        new Object[] { "John", 25, "Dallas" }, Locale.US);

    log.info("Nom utilisateur (France) : " + name);

    RessourceHelperService svc = (RessourceHelperService) context.getBean(
        "ressourceHelperService");
    svc.afficherMessages();
  }
}

package fr.eni.services;

import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.util.Locale;


/**
 * Classe de support pour les ressources
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class RessourceHelperService implements MessageSourceAware {
  /**
   * La source de messages
   */
  private MessageSource messageSource;

  /**
   * Le setteur
   *
   * @param messageSource La source de message
   */
  public void setMessageSource(MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  /**
   * Affichage des messages
   */
  public void afficherMessages() {
    String name = messageSource.getMessage("utilisateur.nom",
        new Object[] { "John", 25, "Dallas" }, Locale.US);

    log.info("User name (English) : " + name);

    name = messageSource.getMessage("utilisateur.nom",
        new Object[] { "John", 25, "Dallas" }, Locale.US);

    log.info("Nom utilisateur (France) : " + name);
  }
}

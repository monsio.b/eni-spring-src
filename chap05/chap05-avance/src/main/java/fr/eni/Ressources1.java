package fr.eni;

import fr.eni.services.ResourceLoaderAndPrinter;
import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.Scanner;


/**
 * Les fichiers de ressources standards
 *
 * @author $author$
 * @version $Revision$
 */
@Slf4j
public class Ressources1 {

    /**
     * Point d'entrée
     *
     * @param args Arguments de la ligne de commande
     * @throws IOException Exception levée
     */
    public static void main(String[] args) throws IOException {

        ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{
                "applicationContext.xml"
        });

        ResourceLoaderAndPrinter resourceLoaderAndPrinter =
                (ResourceLoaderAndPrinter) ctx.getBean("loaderprinter");
        //It's the directory where java was run from, where you started the JVM
        String filePath = System.getProperty("user.dir") +
                "/chap05/chap05-avance/src/main/resources/fr/eni/ressource.txt";

        log.info("Fichier:" + filePath);

        Resource r0 = new FileSystemResource(filePath);
        resourceLoaderAndPrinter.traiterRessource(r0, "FileSystemResource");

        Resource r1 = new UrlResource("file:" + filePath);
        resourceLoaderAndPrinter.traiterRessource(r1, "UrlResource");

        Resource r2 = new ClassPathResource("fr/eni/ressource.txt");
        resourceLoaderAndPrinter.traiterRessource(r2, "ClassPathResource");
    }
}

package fr.eni;

import fr.eni.services.ResourceLoaderAndPrinter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Slf4j
@Getter
public class DataSourceConfig {

    private String url, username, password;

    public DataSourceConfig() {
        log.info("construct no args DataSourceConfig");
    }

    public static void main(String[] args) {

        ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{
                "applicationContext.xml"
        });

        DataSourceConfig dataSourceConfig =
                (DataSourceConfig) ctx.getBean("dataSourceConfig");

        log.info("url : " + dataSourceConfig.url);
        log.info("password : " + dataSourceConfig.password);
        log.info("username : " + dataSourceConfig.username);

    }

    public void setUrl(String url) {
        log.info("setUrl : "+url);
        this.url = url;
    }

    public void setUsername(String username) {
        log.info("setUsername : "+username);
        this.username = username;
    }

    public void setPassword(String password) {
        log.info("setPassword : "+password);
        this.password = password;
    }
}

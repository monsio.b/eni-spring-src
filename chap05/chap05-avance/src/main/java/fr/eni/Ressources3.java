package fr.eni;

import fr.eni.services.ResourceLoaderAndPrinter;
import fr.eni.services.RessourceHelperService;

import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Les fichiers de ressources chargés via
 * un service ResourceLoaderAware
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class Ressources3 {
  /**
   * Point d'entrée
   *
   * @param args Arguments de la ligne de commande
   */
  public static void main(String[] args) {

    ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[] {
          "applicationContext.xml"
        });

    ResourceLoaderAndPrinter resourceLoaderAndPrinter=
        (ResourceLoaderAndPrinter)ctx.getBean("loaderprinter");

    RessourceHelperService svc = (RessourceHelperService) ctx.getBean(
        "ressourceHelperService");

    String rep = System.getProperty("user.dir");
    log.info("Rep courant:" + rep);

    Resource r0 = svc.getResource("file:" + rep +
        "/chap05/chap05-avance/src/main/resources/fr/eni/ressource.txt");
    resourceLoaderAndPrinter.traiterRessource(r0, "getResource avec file:");

    Resource r1 = svc.getResource("classpath:fr/eni/ressource.txt");
    resourceLoaderAndPrinter.traiterRessource(r1, "getResource avec classpath:");

    Resource r2 = svc.getResource(
        "url:http://echo.jsontest.com/key/value/one/two");
    resourceLoaderAndPrinter.traiterRessource(r2, "getResource avec url:");
  }
}

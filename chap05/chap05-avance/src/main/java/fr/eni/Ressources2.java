package fr.eni;

import fr.eni.services.ResourceLoaderAndPrinter;
import lombok.extern.java.Log;

import lombok.extern.slf4j.Slf4j;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Les fichiers de ressources chargés via le contexte
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class Ressources2 {
  /**
   * Point d'entrée
   *
   * @param args Arguments de la ligne de commande
   */
  public static void main(String[] args) {
    ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[] {
          "applicationContext.xml"
        });

    ResourceLoaderAndPrinter resourceLoaderAndPrinter=
        (ResourceLoaderAndPrinter)ctx.getBean("loaderprinter");

    String rep = System.getProperty("user.dir");
    log.info("Rep courant:" + rep);

    Resource r0 = ctx.getResource("file:" + rep +
        "/chap05/chap05-avance/src/main/resources/fr/eni/ressource.txt");
    resourceLoaderAndPrinter.traiterRessource(r0, "getResource avec file:");

    Resource r1 = ctx.getResource("classpath:fr/eni/ressource.txt");
    resourceLoaderAndPrinter.traiterRessource(r1, "getResource avec classpath:");

    Resource r2 = ctx.getResource(
        "url:http://echo.jsontest.com/key/value/one/two");
    resourceLoaderAndPrinter.traiterRessource(r2, "getResource avec url:");
  }
}

package fr.eni.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Slf4j
@Service("loaderprinter")
public class ResourceLoaderAndPrinter {
  /**
   * Traiter la ressource
   *
   * @param resource la ressource
   */
  public void traiterRessource(Resource resource, String type) {
    log.info("============================");
    log.info("type :{}",type);
    try {
      InputStream is = resource.getInputStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      String ligne;

      while ((ligne = br.readLine()) != null) {
        log.info(ligne);
      }

      br.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }}

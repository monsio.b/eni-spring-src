package fr.eni.services;

import org.springframework.context.ResourceLoaderAware;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;


/*
Since bean does not have the application context access, how can a bean access a resources?
The workaround is implement the ResourceLoaderAware interface and create setter method for
ResourceLoader object. Spring will DI the resource loader into your bean.
 */
/**
 * Service d'aide pour les ressources
 *
 * @author $author$
 * @version $Revision$
  */
public class RessourceHelperService implements ResourceLoaderAware {
  /**
   * Le chargeur de ressource
   */
  private ResourceLoader resourceLoader;

  /**
   * Setter du chargeur de ressources
   *
   * @param resourceLoader Le chargeur de ressources
   */
  public void setResourceLoader(ResourceLoader resourceLoader) {
    this.resourceLoader = resourceLoader;
  }

  /**
   * Getter du chargeur de ressources
   *
   * @param location L'emplacement
   *
   * @return le chargeur de ressources
   */
  public Resource getResource(String location) {
    return resourceLoader.getResource(location);
  }
}

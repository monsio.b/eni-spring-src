package fr.eni;

import fr.eni.services.ResourceLoaderAndPrinter;
import fr.eni.services.RessourceHelperService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.net.MalformedURLException;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:applicationContext.xml",
    "classpath:applicationContext-test.xml"})
public class ResourcesTest {

  @Autowired
  ApplicationContext context;

  @Autowired
  ResourceLoaderAndPrinter resourceLoaderAndPrinter;

  @Test
  public void ResTest() throws MalformedURLException {
    String fichier = System.getProperty("user.dir") +
        "/src/main/resources/fr/eni/ressource.txt";
    log.info("Fichier:" + fichier);

    Resource r0 = new FileSystemResource(fichier);
    resourceLoaderAndPrinter.traiterRessource(r0, "FileSystemResource");

    Resource r1 = new UrlResource("file:" + fichier);
    resourceLoaderAndPrinter.traiterRessource(r1, "UrlResource");

    Resource r2 = new ClassPathResource("fr/eni/ressource.txt");
    resourceLoaderAndPrinter.traiterRessource(r2, "ClassPathResource");
  }

  @Test
  public void ResTest2() throws MalformedURLException {
    String rep = System.getProperty("user.dir");
    log.info("Rep courant:" + rep);

    Resource r0 = context.getResource("file:" + rep +
        "/src/main/resources/fr/eni/ressource.txt");
    resourceLoaderAndPrinter.traiterRessource(r0, "getResource avec file:");

    Resource r1 = context.getResource("classpath:fr/eni/ressource.txt");
    resourceLoaderAndPrinter.traiterRessource(r1, "getResource avec classpath:");

    Resource r2 = context.getResource(
        "url:http://echo.jsontest.com/key/value/one/two");
    resourceLoaderAndPrinter.traiterRessource(r2, "getResource avec url:");
  }

  ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{
      "applicationContext.xml"
  });

  @Test
  public void ResTest3() throws MalformedURLException {

    String rep = System.getProperty("user.dir");
    log.info("Rep courant:" + rep);

    Resource r0 = context.getResource("file:" + rep +
        "/src/main/resources/fr/eni/ressource.txt");
    resourceLoaderAndPrinter.traiterRessource(r0, "getResource avec file:");

    Resource r1 = context.getResource("classpath:fr/eni/ressource.txt");
    resourceLoaderAndPrinter.traiterRessource(r1, "getResource avec classpath:");

    Resource r2 = context.getResource(
        "url:http://echo.jsontest.com/key/value/one/two");
    resourceLoaderAndPrinter.traiterRessource(r2, "getResource avec url:");
  }
}

package fr.eni;

/**
 * Tests avec lombok project
 *
 * @author $author$
 * @version $Revision$
  */
public class Main {
  /**
   * Point d'entrée
   *
   * @param args Les arguments de la ligne de commande 
   */
  public static void main(String[] args) {
    Utilisateur utilisateur = new Utilisateur();
    utilisateur.setNom("toto");
    System.out.println(utilisateur.getNom());
    utilisateur = new Utilisateur(1, "prenom", "nom");
    System.out.println(utilisateur.toString());
  }
}

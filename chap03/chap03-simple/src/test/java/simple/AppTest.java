package simple;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Tests Unitaires pour l'application simpliste
 */
public class AppTest 
    extends TestCase
{
    /**
     * Créer le cas de test
     *
     * @param nomDuTest Le nom du cas de test
     */
    public AppTest( String nomDuTest )
    {
        super( nomDuTest );
    }

    /**
     * @return La suite de tests qui doit être testée
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Test pour la forme
     */
    public void testApp()
    {
        assertTrue( true );
    }
}

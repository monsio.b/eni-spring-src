package fr.eni;

import org.h2.tools.DeleteDbFiles;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


/**
 * Connexion H2
 *
 * @author $author$
 * @version $Revision$
  */
public class ConnectH2 {
  /**
   * Point d'entrée
   *
   * @throws Exception Exception levée
   */
  public static void main(String... args) throws Exception {
    //On efface la base
    DeleteDbFiles.execute("~", "test", true);

    //Connexion
    Class.forName("org.h2.Driver");

    Connection connection = DriverManager.getConnection("jdbc:h2:~/test");
    Statement statement = connection.createStatement();

    //Création table
    statement.execute(
      "create table test(id int primary key, name varchar(255))");
    //Insersion d'une ligne
    statement.execute("insert into test values(1, 'ENI Editions')");

    //Lecture de la ligne
    ResultSet rs;
    //Affichage
    rs = statement.executeQuery("select * from test");

    while (rs.next()) {
      System.out.println(rs.getString("name"));
    }

    //Fermeture Statement 
    statement.close();
    //Fermeture base
    connection.close();
  }
}

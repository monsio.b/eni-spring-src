package fr.eni;

import java.io.Serializable;

import java.rmi.dgc.VMID;


//pris ici : https://forum.hibernate.org/viewtopic.php?f=1&t=928172&start=105
/**
 * Modèle amélioré de equals et hascode utilisable avec JPA
 *
 * @author $author$
 * @version $Revision$
  */
public class ModelElabore implements Serializable {
  /**
   * Le vmid
   */
  private volatile VMID vmid;

  /**
   * L'id
   */
  private Long id;

  /**
   * getter du id
   *
   * @return id
   */
  public Long getId() {
    return id;
  }

  /**
   * setter du id
   *
   * @param id id
   */
  private void setId(Long id) {
    this.id = id;
  }

  /**
   * le test d'égalité
   *
   * @param obj l'objet à comparer
   *
   * @return true si objets égaux, false sinon
   */
  public boolean equals(Object obj) {
    final boolean returner;

    if (obj instanceof ModelElabore) {
      return getVmid().equals(((ModelElabore) obj).getVmid());
    } else {
      returner = false;
    }

    return returner;
  }

  /**
   * Calcul du hascode
   *
   * @return Le hascode
   */
  public int hashCode() {
    return getVmid().hashCode();
  }

  /**
   * getter du vmid
   *
   * @return le vmid
   */
  private Object getVmid() {
    if ((vmid != null) || ((vmid == null) && (id == null))) {
      if (vmid == null) { //Avoid the performance impact of synchronized if we can

        synchronized (this) {
          if (vmid == null) {
            vmid = new VMID();
          }
        }
      }

      return vmid;
    }

    return id;
  }
}

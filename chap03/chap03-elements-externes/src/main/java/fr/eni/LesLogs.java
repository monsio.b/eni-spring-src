package fr.eni;

import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Utilisation des logs lombok
 *
 * @author $author$
 * @version $Revision$
  */
@Slf4j
public class LesLogs {
  /**
   * Le logger
   */
  private static final Logger slf4jLogger = LoggerFactory.getLogger(LesLogs.class);

  /**
   * Point d'entrée
   *
   * @param args Les arguments de la ligne de commande 
   */
  public static void main(String[] args) {
    slf4jLogger.trace("Bonjour à vous!");

    String nom = "John CONNOR";
    slf4jLogger.debug("Hi, {}", nom);
    slf4jLogger.info("Log de type info.");
    slf4jLogger.warn("Log de type warn.");
    slf4jLogger.error("Log de type error.");
    log.info("Log de type info via lombok, {}", nom);
  }
}

package fr.eni;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Application minimum
 *
 * @author $author$
 * @version $Revision$
  */
public class Main {
  /**
   * Le logger jcl-over-slf4j
   */
  private static final Logger logger = LoggerFactory.getLogger(Main.class);

	/**
   * Méthode main
   *
   * @param args de la ligne de commande
   */
  public static void main(String[] args) {    
    logger.info("Java Spring");
    logger.debug("Le socle technique des applications Java EE");
  }
}

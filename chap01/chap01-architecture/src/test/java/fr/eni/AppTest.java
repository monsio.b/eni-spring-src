package fr.eni;

import org.junit.Test;

import org.junit.runner.RunWith;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * TU Application minimum
 *
 * @author $author$
 * @version $Revision$
  */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations =  {
  "classpath*:Spring/applicationContext-test.xml"}
)
public class AppTest {
  /**
   * Le logger jcl-over-slf4j
   */
  private static final Logger logger = LoggerFactory.getLogger(AppTest.class);

  /**
   * Tests simples
   */
  @Test
  public void testApp() {
    logger.info("Test Spring mode info");
    logger.debug("Test Spring mode debug");
  }
}

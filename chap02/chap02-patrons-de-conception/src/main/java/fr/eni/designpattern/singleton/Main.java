package fr.eni.designpattern.singleton;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Exemple singleton
 *
 * @author $author$
 * @version $Revision$
  */
public class Main {
  /**
   * Méthode main
   *
   * @param args les arguments de la ligne de commande
   */
  public static void main(String[] args) {
    ApplicationContext context = new ClassPathXmlApplicationContext(
        "applicationContext.xml");

    ServiceUnique s1 = (ServiceUnique) context.getBean("serviceUnique");
    ServiceUnique s2 = (ServiceUnique) context.getBean("serviceUnique");
    s1.setEtat(3);

    System.out.println("Mêmes valeurs (equals) ?: " + s1.equals(s2));
    System.out.println("Même référence (==)    ?: " + (s1 == s2));
    System.out.println(s1);
    System.out.println(s2);

    s1.setEtat(2);
    s2.afficherEtat();
  }
}

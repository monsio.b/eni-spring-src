package fr.eni.designpattern.singleton;

import org.springframework.stereotype.Service;

/**
 * Le singleton
 *
 * @author $author$
 * @version $Revision$
  */
@Service("serviceUnique")
public class ServiceUnique {
  /**
   * Etat du singleton
   */
  public int etat = 0;

  /**
   * Incrémentation synchronisée
   *
   * @return état incrémenté
   */
  public synchronized int incEtat() {
    //Opération longue
    return ++etat;
  }

  /**
   * getter de l'état
   *
   * @return etat
   */
  public int getEtat() {
    return etat;
  }

  /**
   * setter de l'etat (synchronisé)
   *
   * @param l'etat
   */
  public synchronized void setEtat(int etat) {
    this.etat = etat;
  }

  /**
   * Affichage de l'état
   */
  void afficherEtat() {
    System.out.println(etat);
  }

  /**
   * Calcul du hascode
   *
   * @return le hashcode
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + etat;

    return result;
  }

  /**
   * Mise en forme pour affichage
   *
   * @return la chaine formatée.
   */
  @Override
  public String toString() {
    return "ServiceUnique [etat=" + etat + "]";
  }

  /**
   * Egalité
   *
   * @param obj Objet à comparer
   *
   * @return le résultat de la comparaison
   */
  @Override
  public boolean equals(Object obj) {
    ServiceUnique other = (ServiceUnique) obj;

    if (etat != other.etat) {
      return false;
    }

    return true;
  }
}


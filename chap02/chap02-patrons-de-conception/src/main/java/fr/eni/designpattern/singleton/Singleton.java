package fr.eni.designpattern.singleton;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Exemple singleton simple!
 *
 * @author $author$
 * @version $Revision$
  */
public class Singleton {
  /**
   * Point d'entrée
   *
   * @param args Arguments de la ligne de commande
   */
  public static void main(String[] args) {
    ApplicationContext context = new ClassPathXmlApplicationContext(
        "applicationContext.xml");

    String s1 = (String) context.getBean("maString");
    String s2 = (String) context.getBean("maString");

    System.out.println("Mêmes valeurs (equals) ?: " + s1.equals(s2));
    System.out.println("Même référence (==)    ?: " + (s1 == s2));
    System.out.println(s1);
    System.out.println(s2);
  }
}

package fr.eni.proxy;

import org.aopalliance.aop.Advice;

import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.framework.ProxyFactory;

import java.lang.reflect.Method;
import java.lang.reflect.Method;


/**
 * L'interface du pojo simple
 *
 * @author $author$
 * @version $Revision$
  */
interface Pojo3 {
  /**
   * Méthode qui est interceptée
   */
  public void foo();
}


/**
 * Un pojo simple
 *
 * @author $author$
 * @version $Revision$
  */
class SimplePojo3 implements Pojo3 {
  /**
   * Méthode affichant foo.
   */
  public void foo() {
    System.out.println("foo");
  }
}


/**
 * Méthode retry
 *
 * @author $author$
 * @version $Revision$
  */
class RetryAdvice implements AfterReturningAdvice {
  /**
   * Méthode appelée après l'appel normal
   *
   * @param returnValue La nouvelle valeur à retourner
   * @param method La méthode à détourner
   * @param args les arguments de la méthode à détourner
   * @param target Objet cible du détournement
   *
   * @throws Throwable L'exception levée
   */
  public void afterReturning(Object returnValue, Method method, Object[] args,
    Object target) throws Throwable {
    System.out.println("Après " + method.getName());
  }
}


/**
 * La main avec un aspect
 *
 * @author $author$
 * @version $Revision$
  */
public class MainAvecAspect {
  /**
   * Point d'entrée
   *
   * @param args Arguments de la ligne de commande
   */
  public static void main(String[] args) {
    ProxyFactory factory = new ProxyFactory(new SimplePojo3());
    Class<?> intf = Pojo3.class;
    factory.addInterface(intf);

    Advice advice = new RetryAdvice();
    factory.addAdvice(advice);
    factory.setExposeProxy(true);

    Pojo3 pojo = (Pojo3) factory.getProxy();

    pojo.foo();
  }
}

package fr.eni.simples;

import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;


/**
 * Le singleton classique avant les enum
 *
 * @author $author$
 * @version $Revision$
  */
class MonSingleton {
  /**
   * L'instance du singleton
   */
  private static MonSingleton UNIQUE = null;

  /**
   * Création d'un nouveau MonSingleton.
   */
  private MonSingleton() {
  }

  /**
   * Retourne l'instance du singleton, si il n'existe pas encore le créé
   *
   * @return L'instance du singleton
   */
  public static synchronized MonSingleton getInstance() {
    if (UNIQUE == null) {
      UNIQUE = new MonSingleton();
    }

    return UNIQUE;
  }
}


/**
 * Point d'entrée
 *
 * @author $author$
 * @version $Revision$
  */
@Log
public class Main {
  /**
   * Point d'entrée
   *
   * @param args Arguments de la ligne de commande
   */
  public static void main(String[] args) {
    MonSingleton s1 = MonSingleton.getInstance();
    MonSingleton s2 = MonSingleton.getInstance();

    boolean test = s1 == s2;
    log.info("s1==s2:" + test);
  }
}

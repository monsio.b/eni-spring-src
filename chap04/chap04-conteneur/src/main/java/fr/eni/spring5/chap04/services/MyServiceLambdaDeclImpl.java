package fr.eni.spring5.chap04.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
public class MyServiceLambdaDeclImpl implements MyService{
  public void myMethod(String msg) {
    log.info(msg);
  }
}

package fr.eni.spring5.chap04;

import fr.eni.spring5.chap04.services.MyService;
import fr.eni.spring5.chap04.services.MyServiceLambdaDeclImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.ClassPathResource;

@Slf4j
public class Chap04ConteneurApplication {

    public static void main(String[] args) {
        log.info("Chapitre 4");

        GenericApplicationContext genericApplicationContext = new GenericApplicationContext();
        XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(genericApplicationContext);
        xmlReader.loadBeanDefinitions(new ClassPathResource("applicationContext.xml"));
        genericApplicationContext.refresh();

        //Déclaré dans le fichier xml
        MyService myServiceByXml = (MyService) genericApplicationContext.getBean("myServiceByXml");
        myServiceByXml.myMethod("Déclaré dans le fichier xml");

        //Déclaré via une annotation
        MyService myServiceByAnnotation = (MyService) genericApplicationContext.getBean("myServiceByAnnotation");
        myServiceByAnnotation.myMethod("Déclaré via une annotation");

        //Déclaré par @Configuration
        MyService myServiceByConfiguration = (MyService) genericApplicationContext.getBean("myServiceByConfiguration");
		myServiceByConfiguration.myMethod("Déclaré via une classe @Configuration");

        //Déclaré par une lambda
        genericApplicationContext.registerBean(MyServiceLambdaDeclImpl.class, () -> new MyServiceLambdaDeclImpl());
        MyServiceLambdaDeclImpl myServiceByLambda = (MyServiceLambdaDeclImpl) genericApplicationContext.getBean("fr.eni.spring5.chap04.services.MyServiceLambdaDeclImpl");
        myServiceByLambda.myMethod("Déclaré via une lambda");

        String[] beanNames = genericApplicationContext.getBeanDefinitionNames();
        for (String beanName : beanNames) {
            log.info("Bean : {}:{} ", beanName, genericApplicationContext.getBean(beanName).getClass().toString());
        }
    }
}

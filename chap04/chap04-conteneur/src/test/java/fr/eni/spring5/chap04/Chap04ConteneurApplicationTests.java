package fr.eni.spring5.chap04;

import fr.eni.spring5.chap04.services.MyService;
import fr.eni.spring5.chap04.services.MyServiceLambdaDeclImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:applicationContext.xml",
    "classpath:applicationContext-test.xml"})
public class Chap04ConteneurApplicationTests {

  @Test
  public void test() {
    log.info("Chapitre 4");

    //Le GenericApplicationContext est requis pour pouvoir chaerger via une lambda
    //Sinon le Generic suffit
    GenericApplicationContext genericApplicationContext = new GenericApplicationContext();
    XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(genericApplicationContext);
    xmlReader.loadBeanDefinitions(new ClassPathResource("applicationContext.xml"));
    genericApplicationContext.refresh();


    //Déclaré dans le fichier xml
    MyService myServiceByXml=(MyService)genericApplicationContext.getBean("myServiceByXml");
    myServiceByXml.myMethod("Déclaré dans le fichier xml");

    //Déclaré via une annotation
    MyService myServiceByAnnotation=(MyService)genericApplicationContext.getBean("myServiceByAnnotation");
    myServiceByAnnotation.myMethod("Déclaré via une annotation");

    //Déclaré par @Configuration
    MyService myServiceByConfiguration=(MyService)genericApplicationContext.getBean("myServiceByConfiguration");
    myServiceByAnnotation.myMethod("Déclaré via une classe @Configuration");

    //Déclaré par une lambda
    genericApplicationContext.registerBean(MyServiceLambdaDeclImpl.class, () -> new MyServiceLambdaDeclImpl());
    MyServiceLambdaDeclImpl myServiceByLambda=(MyServiceLambdaDeclImpl)genericApplicationContext.getBean("fr.eni.spring5.chap04.services.MyServiceLambdaDeclImpl");
    myServiceByLambda.myMethod("Déclaré via une lambda");
  }
}
